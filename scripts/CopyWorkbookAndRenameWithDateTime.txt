Sub CopyWorkbookAndRenameWithDateTime()
    Dim sourceWorkbook As Workbook
    Dim destPath As String
    Dim destFileName As String
    Dim currentDateTime As String
    
    ' Define the path to the source workbook
    Dim sourceWorkbookPath As String
    sourceWorkbookPath = "C:\Path\To\SourceWorkbook.xlsx" ' Replace with the actual path
    
    ' Define the folder where you want to copy the workbook
    destPath = "C:\Path\To\Destination\Folder\" ' Replace with the desired folder path
    
    ' Get the current date and time as a string
    currentDateTime = Format(Now, "yyyy-mm-dd_hhmmss")
    
    ' Open the source workbook
    On Error Resume Next
    Set sourceWorkbook = Workbooks.Open(sourceWorkbookPath)
    On Error GoTo 0
    
    If Not sourceWorkbook Is Nothing Then
        ' Define the destination file name with the current date and time
        destFileName = destPath & "Copy_" & currentDateTime & ".xlsx"
        
        ' Save a copy of the source workbook with the new name
        sourceWorkbook.SaveCopyAs destFileName
        
        ' Close the source workbook without saving changes
        sourceWorkbook.Close SaveChanges:=False
        
        ' Notify the user that the copy and rename are complete
        MsgBox "Workbook has been copied and renamed with the current date and time in the specified folder.", vbInformation
    Else
        MsgBox "The source workbook could not be opened.", vbExclamation
    End If
End Sub
