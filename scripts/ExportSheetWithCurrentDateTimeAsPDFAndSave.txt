Sub ExportSheetWithCurrentDateTimeAsPDFAndSave()
    Dim otherWorkbook As Workbook
    Dim targetSheet As Worksheet
    Dim pdfFileName As String
    Dim savePath As String
    Dim currentDateTime As String
    
    ' Define the path to the other workbook
    Dim otherWorkbookPath As String
    otherWorkbookPath = "C:\Path\To\OtherWorkbook.xlsx" ' Replace with the actual path
    
    ' Define the name of the target sheet
    Dim sheetName As String
    sheetName = "Sheet1" ' Replace with the actual sheet name
    
    ' Define the folder where you want to save the PDF and the workbook
    savePath = "C:\Path\To\Save\Folder\" ' Replace with the desired folder path
    
    ' Get the current date and time as a string
    currentDateTime = Format(Now, "yyyy-mm-dd_hhmmss")
    
    ' Open the other workbook
    On Error Resume Next ' In case the workbook is already open
    Set otherWorkbook = Workbooks.Open(otherWorkbookPath)
    On Error GoTo 0
    
    If Not otherWorkbook Is Nothing Then
        ' Check if the target sheet exists
        On Error Resume Next
        Set targetSheet = otherWorkbook.Sheets(sheetName)
        On Error GoTo 0
        
        If Not targetSheet Is Nothing Then
            ' Define the PDF file name with the current date and time
            pdfFileName = savePath & otherWorkbook.Name & "_" & sheetName & "_" & currentDateTime & ".pdf"
            
            ' Export the target sheet as PDF
            targetSheet.ExportAsFixedFormat Type:=xlTypePDF, FileName:=pdfFileName
            
            ' Save the other workbook
            otherWorkbook.Save
            
            ' Close the other workbook without saving changes
            otherWorkbook.Close SaveChanges:=False
            
            ' Notify the user that the export is complete
            MsgBox "Sheet exported as PDF and workbook saved in the specified folder.", vbInformation
        Else
            MsgBox "The target sheet '" & sheetName & "' does not exist in the other workbook.", vbExclamation
        End If
    Else
        MsgBox "The other workbook could not be opened.", vbExclamation
    End If
End Sub
